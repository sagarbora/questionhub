<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateQuestionRequest;
use App\Http\Requests\UpdateQuestionRequest;
use App\Models\Question;
use Illuminate\Routing\Controllers\HasMiddleware;
use Illuminate\Routing\Controllers\Middleware;
use Illuminate\Support\Facades\Gate;

class QuestionsController extends Controller
implements HasMiddleware
{
    public static function middleware(): array {
        return [
            new Middleware('auth', only:['store', 'create']),
            new Middleware('trackQuestionViews', only:['show'])

        ];
    }
    public function index(){
        $questions = Question::with('owner')->latest()->paginate(30);
        return view('qa.questions.index', compact(['questions']));
    }

    public function create() {
        // dd( auth()->user()->questions());
        return view('qa.questions.create');
    }

    public function store(CreateQuestionRequest $request) {
      auth()->user()->questions()->create([
            'title'=>$request->title,
            'body' => $request->body
        ]);
        // dd(auth()->user());

        session()->flash('success', 'Question has been added successfully!');
        return redirect(route('questions.index'));
    }
    public function update( UpdateQuestionRequest $request, Question $question) {
        $question->update([
            'title'=>$request->title,
            'body' => $request->body
        ]);
        session()->flash('success', 'Question has been updated successfully');
        return redirect(route('questions.index'));
    }

    public function destroy(Question $question) {
        Gate::authorize('delete',$question);
        $question->delete();
        session()->flash('success', 'Question has been deleted successfully!');
        return redirect(route('questions.index'));

    }

    public function edit(Question $question){
        //old method
         // if(! Gate::allows('update', $question)){
        //     abort(403);
        // }

        //new method
        Gate::authorize('update', $question);
        return view('qa.questions.edit', compact(['question']));
    }
    public function show(Question $question) {
    // dd('hello');
        return view('qa.questions.show', compact(['question']));
    }
}
