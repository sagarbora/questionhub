@extends('qa.layouts.app')
@section('page-level-styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css" integrity="sha512-SnH5WK+bZxgPHs44uWIX+LLJAJ9/2PkPKZ5QiAj6Ta86w+fsb2TkcmfRyVX3pBnMFcV7oQPJkl9QevSCWr3W6A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" type="text/css" href="https://unpkg.com/trix@2.0.8/dist/trix.css">
@endsection

@section('page-level-script')
    <script type="text/javascript" src="https://unpkg.com/trix@2.0.8/dist/trix.umd.min.js"></script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h1>
                            {{ $question->title}}
                        </h1>
                    </div>
                    <div class="card-body">
                        <p>
                            {!! $question->body !!}
                        </p>
                    </div>

                    <div class="card-footer">
                        <div class="d-flex justify-content-between mr-3">
                            <div class="d-flex">
                                <div class="me-3">
                                    <a href="#" title="Up Vote" class="vote-up d-block text-center text-dark">
                                        <i class="fa fa-caret-up fa-3x"></i>
                                    </a>
                                    <h4 class="votes-count textmuted text-center m-0">{{ $question->votes_count }} </h4>
                                    <a href="#" title="Down Vote" class="vote-up d-block text-center text-dark">
                                        <i class="fa fa-caret-down fa-3x"></i>
                                    </a>
                                </div>
                                <div class="ml-4 mt-2">
                                    <a href="#" title="Mark as Fav">
                                        <i class="fa fa-star fa-2x text-dark"></i>
                                    </a>
                                    <h4>123</h4>
                                </div>
                            </div>
                            <div class="d-flex flex-column">
                                <div class="text-muted mb-2 text-right">
                                    Asked At: {{$question->create_date}}
                                </div>
                                <div class="d-flex mb-2">
                                    <div class="me-2">
                                        <img src="{{ $question->owner->avatar }}" alt="Avatar of {{ $question->owner->name }}"  >
                                    </div>
                                    <div class="mt-2 ml-2">
                                        {{ $question->owner->name}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @auth
            @include('qa.answers.partials._create')
        @endauth
        @include('qa.answers.partials._index')
    </div>

@endsection
